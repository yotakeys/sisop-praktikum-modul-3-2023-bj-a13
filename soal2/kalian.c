#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main(int argc, char const *argv[])
{
    /* code */

    int matrix1[4][2];
    int matrix2[2][5];
    
    //seed rand() number
    srand(time(NULL));

    //input first matrix
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 2; j++){
            matrix1[i][j] = (rand() % 5) + 1;
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    printf("\n");
    //input second matrix
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 5; j++){
            matrix2[i][j] = (rand() % 4) + 1;
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }

    //matrix multiplication
    //put the result in shared memory
    int size = 20 * sizeof(int);
    key_t key = 1234;
    int shmid = shmget(key, size, IPC_CREAT | 0666);
    int* shared_result = (int*) shmat(shmid, NULL, 0);

    // calculate matrix, save result to shared memory
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int sum = 0;
            for (int k = 0; k < 2; k++) {
                sum += matrix1[i][k] * matrix2[k][j];
            }
            *(shared_result + i * 5 + j) = sum;
        }
    }

    printf("\nResult matrix:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", *(shared_result + i * 5 + j));
        }
        printf("\n");
    }

    shmdt(shared_result);

    printf("run cinta.c and sisop.c in another terminal while kalian.c is running");
    //wait for cinta.c and sisop.c
    sleep(10);

    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
