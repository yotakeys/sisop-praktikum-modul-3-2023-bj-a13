#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>

#define MAX_DIGITS 140

typedef struct {
    int row_index;
    int* shared_matrix;
    char (*factorials)[5][MAX_DIGITS];
} thread_args;

void factorial(int n, char* result) {

    // initial result is set to 1
    memset(result, '0', MAX_DIGITS);
    result[MAX_DIGITS - 1] = '1';

    // calculate factorial
    for (int i = 2; i <= n; i++) {
        int carry = 0;
        for (int j = MAX_DIGITS - 1; j >= 0; j--) {
            int digit = (result[j] - '0') * i + carry;
            result[j] = digit % 10 + '0';
            carry = digit / 10;
        }
    }

    // trim zeros
    int i = 0;
    while (result[i] == '0' && i < MAX_DIGITS - 1) {
        i++;
    }

    // Shift the digits to the beginning of the array.
    if (i > 0) {
        for (int j = i; j < MAX_DIGITS; j++) {
            result[j - i] = result[j];
        }
        memset(result + MAX_DIGITS - i, 0, i);
    }
}

void* calculate_factorials(void* args) {
    thread_args* t_args = (thread_args*) args;
    int row_index = t_args->row_index;
    int* shared_matrix = t_args->shared_matrix;
    char (*factorials)[5][MAX_DIGITS] = t_args->factorials;

    printf("Thread %ld is running\n", pthread_self());

    // Calculate the factorial of each element in the row and store it in factorials.
    for (int j = 0; j < 5; j++) {
        int n = *(shared_matrix + row_index * 5 + j);
        if (n > 0 && n <= 40) {
            factorial(n, factorials[row_index][j]);
        }
    }

    return NULL;
}

int main(int argc, char const *argv[]) {
    // Start measuring time.
    clock_t start = clock();

    key_t key = 1234;
    int shmid;
    int *shared_matrix;
    char factorials[4][5][MAX_DIGITS];
    pthread_t threads[4];

    //get shared matrix from the shared memory id 1234
    shmid = shmget(key, sizeof(int) * 4 * 5, 0666);
    shared_matrix = shmat(shmid, NULL, 0);

    //make 4 threads, then each thread calculate factorial of each rows
    thread_args t_args[4];
    for (int i = 0; i < 4; i++) {
        t_args[i].row_index = i;
        t_args[i].shared_matrix = shared_matrix;
        t_args[i].factorials = factorials;
        pthread_create(&threads[i], NULL, calculate_factorials, &t_args[i]);
    }

    // Wait for threads to finish -> join
    for (int i = 0; i < 4; i++) {
        pthread_join(threads[i], NULL);
    }

    // Print factorials
    printf("\nFactorials:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int n = *(shared_matrix + i * 5 + j);
            if (n > 0 && n <= 40) {
                printf("\n%d! = %s", n, factorials[i][j]);
            }
        }
    }
    printf("\n");

    shmdt(shared_matrix);

    clock_t end = clock();
    double time_spent = (double) (end - start) / CLOCKS_PER_SEC;   
    
    printf("Total time taken: %f seconds\n", time_spent);
    return 0;
}
