#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#define MAX_DIGITS 140

void factorial(int n, char* result) {
    // initial result is set to 1
    memset(result, '0', MAX_DIGITS);
    result[MAX_DIGITS - 1] = '1';

    // calculate factorial (loop way)
    for (int i = 2; i <= n; i++) {

        int carry = 0;
        for (int j = MAX_DIGITS - 1; j >= 0; j--) {
            int digit = (result[j] - '0') * i + carry;
            result[j] = digit % 10 + '0';
            carry = digit / 10;
        }
    }

    //trim zeros
    int i = 0;
    while (result[i] == '0' && i < MAX_DIGITS - 1) {
        i++;
    }

    // Shift the digits to the beginning of the array.
    if (i > 0) {
        for (int j = i; j < MAX_DIGITS; j++) {
            result[j - i] = result[j];
        }
        memset(result + MAX_DIGITS - i, 0, i);
    }
}

int main(int argc, char const *argv[]) {
    // Start measuring time.
    clock_t start = clock();

    //key for shared memory
    key_t key = 1234;
    int shmid;
    int *shared_matrix;
    char factorials[4][5][MAX_DIGITS];

    //get shared_matrix from shared memory
    shmid = shmget(key, sizeof(int) * 20, 0666);
    shared_matrix = shmat(shmid, NULL, 0);

    // Calculate the factorial of each element in shared_matrix and store it in factorials.
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int n = *(shared_matrix + i * 4 + j);
            if (n > 0 && n <= 40) {
                factorial(n, factorials[i][j]);
            }
        }
    }

    // Print the factorials.
    printf("\nFactorials:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int n = *(shared_matrix + i * 5 + j);
            if (n > 0 && n <= 40) {
                printf("%d! = %s\n", n, factorials[i][j]);
            }
        }
    }

    printf("\n");

    shmdt(shared_matrix);

    clock_t end = clock();
    double time_spent = (double) (end - start) / CLOCKS_PER_SEC;   
    
    printf("Total time taken: %f seconds\n", time_spent);
    return 0;
}