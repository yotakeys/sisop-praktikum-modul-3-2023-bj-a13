# Sisop Praktikum Modul 3 2023 BJ-A13

Anggota :

1. Keyisa Raihan I. S. (5025211002)
2. M. Taslam Gustino (5025211011)
3. Adrian Karuna S. (5025211019)
4. Vito Febrian Ananta (5025211224)

## Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut!

Catatan: 
- Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
        - Huruf A	: 01000001 
        - Huruf a	: 01100001 
- Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya 
- Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

### Bagian A
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

Bagian ini dapat menyelesaikannya dengan cara sebagai berikut:
- Baca file.txt:
    - menggunakan I/O dari library stdio.h untuk membuka file.txt
    ```
    // Open input file
    FILE *input_file = fopen(INPUT_FILE, "r");
    if (input_file == NULL)
    {
        printf("Failed to open input file\n");
        exit(1);
    }
    ```
- Hitung frekuensi huruf-huruf di dalam file tersebut:
    - melakukan perulangan sampai akhir file.txt dengan menggunakan fungsi fgetc() dari library stdio.h dan isalpha() untuk mendapatkan setiap karakter dalam file.txt serta melakukan perubahan huruf yang awalnya lowercase menjadi uppercase dengan fungsi toupper() dan juga menghitungkan frekuensinya ke dalam variabel freq_table
    ```
    // Initialize frequency table
    int freq_table[26] = {0};

    // Read input file and update frequency table
    int c;
    while ((c = fgetc(input_file)) != EOF)
    {
        if (isalpha(c))
        {
            c = toupper(c);
            freq_table[c - 'A']++;
        }
    }

    // Close input file
    fclose(input_file);

    // Create arrays to store characters and their frequencies
    int size = 0;
    for (int i = 0; i < 26; i++)
    {
        if (freq_table[i] > 0)
        {
            size++;
        }
    }
    char array1[size];
    int array2[size];
    int j = 0;
    for (int i = 0; i < 26; i++)
    {
        if (freq_table[i] > 0)
        {
            array1[j] = 'A' + i;
            array2[j] = freq_table[i];
            j++;
        }
    }
    ```
- Kirim hasil perhitungan ke child process:
    - Menggunakan pipes parent_ke_child[1] (1 untuk write):
    ```
    // Close unused ends of the 1st pipe
        close(parent_to_child_pipe[0]);

        // Send size and arrays to child process
        if (write(parent_to_child_pipe[1], &size, sizeof(size)) < 0 ||
            write(parent_to_child_pipe[1], array1, sizeof(array1)) < 0 ||
            write(parent_to_child_pipe[1], array2, sizeof(array2)) < 0)
        {
            printf("Failed to send data to child process\n");
            exit(1);
        }
    ```

### Bagian B
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima. 

Bagian ini dapat menyelesaikannya dengan cara sebagai berikut:
- Menerima hasil perhitungan frekuensi dari parent process:
    - Menggunakan pipes parent_ke_child[0] (0 untuk read):
    ```
    // Close unused ends of the 1st pipe
    close(parent_to_child_pipe[1]);

    // Read size of arrays from parent process
    int size;
    if (read(parent_to_child_pipe[0], &size, sizeof(size)) < 0)
    {
        printf("Failed to read size from parent process\n");
        exit(1);
    }

    // Read arrays from parent process
    char array1[size];
    int array2[size];
    if (read(parent_to_child_pipe[0], array1, sizeof(array1)) < 0 ||
        read(parent_to_child_pipe[0], array2, sizeof(array2)) < 0)
    {
        printf("Failed to read arrays from parent process\n");
        exit(1);
    }
    ```
- Kompres file:
    - Membuka file.txt dan bits_comprresed.txt (sebagai file.txt yang berisi teks dengan kriteria yang diminta)
    ```
    // Open text files that need to be compressed
    FILE *inputFile = fopen(INPUT_FILE, "rt");
    if (NULL == inputFile)
    {
        printf("ERROR: cannot open the file: %s\n", INPUT_FILE);
        return -1;
    }

    // Open text files that already match the requirement
    FILE *outputFile = fopen(OUTPUT_FILE, "wt");
    if (NULL == outputFile)
    {
        printf("ERROR: cannot open the file: %s\n", OUTPUT_FILE);
        return -1;
    }
    ```
    - Menulis teks ke dalam bits_comprresed.txt:
    ```
    // Write text files that match the requirement
    char line[1000];
    while (fgets(line, sizeof(line), inputFile))
    {
        for (int i = 0; line[i] != '\0'; i++)
        {
            if (isalpha(line[i]) && islower(line[i]))
            {
                fputc(toupper(line[i]), outputFile);
            }
            else if (isalpha(line[i]) && isupper(line[i]))
            {
                fputc(line[i], outputFile);
            }
        }
    }

    // Close the IO
    fclose(inputFile);
    fclose(outputFile);
    ```
    - Mengubah teks dalam bits_comprresed.txt menjadi string ke dalam variabel str
    ```
    // Convert text files to a string
    char str[1000];
    FILE *fp = fopen(OUTPUT_FILE, "r");
    fgets(str, sizeof(str), fp);
    fclose(fp);

    // Close unused ends of the 2nd pipe
    close(child_to_parent_pipe[0]);
    ```
    - Melakukan kompresi:
    ```
    // Compress the string/text with the Huffman Algorithm
    char* encodedStr = HuffmanCodes(array1, array2, size, str);
    ```

### Bagian C
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman

Bagian ini dapat menyelesaikannya dengan cara sebagai berikut:
- Simpan huffman tree ke dalam file tidak dilakukan karena sesuai perintah yang pernah diberikan di discord, tetapi jika huffman diperlukan maka:
    - Menambahkan fungsi printHCodes() di dalam fungsi HuffmanCodes. Berikut adalah contohnya:
    ```
    char* HuffmanCodes(char item[], int freq[], int size, char str[])
    {
        struct MinHNode *root = buildHuffmanTree(item, freq, size);

        int arr[50], top = 0;
        static char huffmanCode[MAX_TREE_HT];
        int index = 0;
        char* result = (char*) malloc(sizeof(char) * strlen(str) * MAX_TREE_HT);
        strcpy(result, "");
        for (int i = 0; str[i] != '\0'; ++i)
        {
            int found = 0;
            encodeCharacter(root, str[i], huffmanCode, 0, &found);
            if (!found)
            {
                printf("Cannot find Huffman code for character: %c\n", str[i]);
                return NULL;
            }
            strcat(result, huffmanCode);
        }
        printf("\nCharacter and Huffmancode:\n");
        printHCodes(root, arr, top);
        return result;
    }
    ```
- Ubah karakter menjadi kode Huffman:
    - Menggunakan fungsi encodeCharacter() yang berada di dalam fungsi HuffmanCodes dengan cara melakukan perulangan pada setiap karakter dalam variabel str dan mencari kode huffman dari karakter yang didapatkan di perulangan tersebut, lalu me-return-nya ke string encodedStr dengan fungsi strcat():
    ```
    void encodeCharacter(struct MinHNode *root, char character, char huffmanCode[], int index, int *found)
    {
        if (root == NULL)
        {
            return;
        }
        if (root->left == NULL && root->right == NULL)
        {
            if (root->item == character)
            {
                huffmanCode[index] = '\0';
                *found = 1;
            }
            return;
        }
        huffmanCode[index] = '0';
        encodeCharacter(root->left, character, huffmanCode, index + 1, found);
        if (*found == 0)
        {
            huffmanCode[index] = '1';
            encodeCharacter(root->right, character, huffmanCode, index + 1, found);
        }
    }

    char* HuffmanCodes(char item[], int freq[], int size, char str[])
    {
        struct MinHNode *root = buildHuffmanTree(item, freq, size);

        static char huffmanCode[MAX_TREE_HT];
        int index = 0;
        char* result = (char*) malloc(sizeof(char) * strlen(str) * MAX_TREE_HT);
        strcpy(result, "");
        for (int i = 0; str[i] != '\0'; ++i)
        {
            int found = 0;
            encodeCharacter(root, str[i], huffmanCode, 0, &found);
            if (!found)
            {
                printf("Cannot find Huffman code for character: %c\n", str[i]);
                return NULL;
            }
            strcat(result, huffmanCode);
        }
        return result;
    }
    ```

### Bagian D
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

Bagian ini dapat menyelesaikannya dengan cara sebagai berikut:
- Kirim encodedStr ke parent process:
    - Menggunakan pipes child_to_parrent[1]:
    ```
    // Send the comprresed string/text to parrent process
    if (write(child_to_parent_pipe[1], encodedStr, strlen(encodedStr) + 1) < 0)
    {
        printf("Failed to send data to parent process\n");
        exit(1);
    }

    // Close pipes
    close(parent_to_child_pipe[0]);
    close(child_to_parent_pipe[1]);
    ```

### Bagian E
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

Bagian ini dapat menyelesaikannya dengan cara sebagai berikut:
- Menghitung total bit sebelum kompresi:
    - Membuka bits_comprresed.txt untuk menghitung total karakter:
    ```
    // Open text file that already match the requirement
    FILE *fp;
    char filename[] = "output.txt";
    int countBefore = 0;
    char ch;

    fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("Unable to open file\n");
        return 0;
    }

    // Count the total character inside the text file
    while ((ch = fgetc(fp)) != EOF)
    {
        countBefore++;
    }

    fclose(fp);
    ```
    - Melakukan perhitungan total bit (perhitungan dilakukan dengan cara mengkalikan total karakter tersebut dengan ASCII BITS yang sudah di define di header lossless.c):
    ```
    int totalBitBefore = countBefore * ASCII_BITS;
    ```
- Menghitung total bit sesudah kompresi:
    - Melakukan perhitungan total bit (perhitungan dilakukan dengan cara menghitung total karakter yang ada pada variabel encodedStr):
    ```
    // Read the encoded string from the child process
    char encodedStr[3000];
    int bytesRead = read(child_to_parent_pipe[0], encodedStr, sizeof(encodedStr));
    if (bytesRead < 0)
    {
        printf("Failed to read string from child process\n");
        exit(1);
    }

    // Trim the encoded string to the actual number of bytes read
    encodedStr[bytesRead] = '\0';

    // printf("\nThis is a string in the parrent process: %s\n", encodedStr);

    int countAfter = 0;  
    
    // Count the total character inside the comprresed string/text
    for(int i = 0; i < strlen(encodedStr); i++) {  
        if(encodedStr[i] != ' ')  
            countAfter++;  
    }  
    ```
- Menampilkan perbandingan total bit:
    - Print variabel totalBitBefore dan variabel totalBitAfter:
    ```
    // Comparing the total bits before compressing and after compressing
    int totalBitBefore = countBefore * ASCII_BITS;
    printf("Before Compression:\n");
    printf("Total bit: %d\n\n", totalBitBefore);

    int totalBitAfter = countAfter;
    printf("After Compression:\n");
    printf("Total bit: %d\n", totalBitAfter);
    ```

### Kendala
- Memahami lagi konsep pipes dengan fork
- Memahami Huffman Coding Algorithm terlebih dahulu
- Menambahkan fungsi encodeCharacter() untuk mengubah setiap karakter dalam file.txt menjadi Huffman Code

### Output
![Output_Soal_1_Modul_3](/uploads/d2fa34a5aa3a3d61e347bee54bcc0821/Output_Soal_1_Modul_3.png)

## Soal 2

### Bagian A

### Kalian.c

membuat program untuk melakukan perkalian matrix 4x2 dengan 2x5, dan setiap elemen matrix adalah angka random dari 1-5 untuk matrix pertama dan 1-4 untuk matrix kedua.

```c

    int matrix1[4][2];
    int matrix2[2][5];

       //seed rand() number
    srand(time(NULL));


        //input first matrix
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 2; j++){
            matrix1[i][j] = (rand() % 5) + 1;
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    printf("\n");
    //input second matrix
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 5; j++){
            matrix2[i][j] = (rand() % 4) + 1;
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }
```

- potongan kode diatas adalah bagian inisialisasi matrix1 dan 2, dan proses input menggunakan rand(), dimana rand() dapat di modulo 4 atau 5 untuk menentukan batas maksimal
- srand(time(NULL)); adalah untuk seeding fungsi rand() supaya tiap run, maka nilai random akan berubah (tidak tetap)

```c

    //matrix multiplication
    //put the result in shared memory
    int size = 20 * sizeof(int);
    key_t key = 1234;
    int shmid = shmget(key, size, IPC_CREAT | 0666);
    int* shared_result = (int*) shmat(shmid, NULL, 0);
```

- disini adalah bagian pembuatan shared memory segment dengan menggunakan shmget dimana key saya set 1234 dan ukurannya adalah 20 integer
- kemudian dibuat sebuah array bernama shared_result yang terletak pada shared memory menggunakan shmat

```c

    // calculate matrix, save result to shared memory
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int sum = 0;
            for (int k = 0; k < 2; k++) {
                sum += matrix1[i][k] * matrix2[k][j];
            }
            *(shared_result + i * 5 + j) = sum;
        }
    }

    printf("\nResult matrix:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", *(shared_result + i * 5 + j));
        }
        printf("\n");
    }

```

- pada bagian ini dilakukan perkalian 2 matrix (matrix 1 & matrix 2), menggunakan 3 loop, kemudian tiap perkalian dimasukkan kedalam shared_result matrix.
- dibawahnya adalah print matrix untuk mengecek hasil perkalian, digunakan nested loop untuk print integer dari shared_result matrix. `c (shared_result + i * 5 + j) ` digunakan karena ini adalah matrix 1 dimensi.

### Bagian B dan C

### Cinta.c

Pada program ini kita perlu mengambil matrix hasil perkalian dari program sebelumnya, lalu melakukan perhitungan faktorial dari tiap elemen matrix dengan menggunakan konsep multithreading.

```c
    // Start measuring time.
    clock_t start = clock();

    key_t key = 1234;
    int shmid;
    int *shared_matrix;
    char factorials[4][5][MAX_DIGITS];
    pthread_t threads[4];

    //get shared matrix from the shared memory id 1234
    shmid = shmget(key, sizeof(int) * 4 * 5, 0666);
    shared_matrix = shmat(shmid, NULL, 0);
```

- clock start adalah untuk mengambil timestamp dimana program dimulai, ini akan digunakan untuk menghitung waktu jalan program
- kemudian disini juga seperti pada sebelumnya dibuat matrix baru dan diisikan shared_matrix dari program sebelumnya dengan menggunakan key/id yang sama (pada shmget, shmat).
- array char factorials digunakan untuk menampung hasil factorial nanti. digunakan char karena tidak ada data type yang cukup untuk menampung 20! keatas

```c
    typedef struct {
        int row_index;
        int* shared_matrix;
        char (*factorials)[5][MAX_DIGITS];
    } thread_args;

    void* calculate_factorials(void* args) {
    thread_args* t_args = (thread_args*) args;
    int row_index = t_args->row_index;
    int* shared_matrix = t_args->shared_matrix;
    char (*factorials)[5][MAX_DIGITS] = t_args->factorials;

    printf("Thread %ld is running\n", pthread_self());

    // Calculate the factorial of each element in the row and store it in factorials.
    for (int j = 0; j < 5; j++) {
        int n = *(shared_matrix + row_index * 5 + j);
        if (n > 0 && n <= 40) {
            factorial(n, factorials[row_index][j]);
        }
    }

    return NULL;
}

    //make 4 threads, then each thread calculate factorial of each rows
    thread_args t_args[4];
    for (int i = 0; i < 4; i++) {
        t_args[i].row_index = i;
        t_args[i].shared_matrix = shared_matrix;
        t_args[i].factorials = factorials;
        pthread_create(&threads[i], NULL, calculate_factorials, &t_args[i]);
    }
```

- disini ada struct bernama thread_args yang akan berfungsi untuk menampung thread input
- yang penting untuk diperhatikan pada struct ini adalah "row_index"
- dapat dilihat pada loop dibawah struct, dimasukkan row_index sesuai dengan i loops saat itu, maka akan dibuat 4 row sesuai dengan ukuran matrix (4 row)
- lalu kita membuat thread sebanyak 4, idenya adalah menggunakan 1 thread per row matrix dalam melakukan perhitungan faktorial.
- didalam pthread_create kita menjalankan calculate_factorials dimana pada dasarnya hanyalah membagi tugas dan assign untuk perhitungan faktorial dengan input berapa dan destinasi jawabannya

```c
   for (int j = 0; j < 5; j++) {
        int n = *(shared_matrix + row_index * 5 + j);
        if (n > 0 && n <= 40) {
            factorial(n, factorials[row_index][j]);
        }
    }
```

- dapat dilihat n akan digunakan sebagai input
- lalu terdapat if yang hanya digunakan untuk memastikan input tidak lebih dari 40 (karena dengan elemen yang ditentukan hanya 40! yang terbesar)

```c
void factorial(int n, char* result) {

    // initial result is set to 1
    memset(result, '0', MAX_DIGITS);
    result[MAX_DIGITS - 1] = '1';

    // calculate factorial
    for (int i = 2; i <= n; i++) {
        int carry = 0;
        for (int j = MAX_DIGITS - 1; j >= 0; j--) {
            int digit = (result[j] - '0') * i + carry;
            result[j] = digit % 10 + '0';
            carry = digit / 10;
        }
    }

    // trim zeros
    int i = 0;
    while (result[i] == '0' && i < MAX_DIGITS - 1) {
        i++;
    }

    // Shift the digits to the beginning of the array.
    if (i > 0) {
        for (int j = i; j < MAX_DIGITS; j++) {
            result[j - i] = result[j];
        }
        memset(result + MAX_DIGITS - i, 0, i);
    }
}
```

- disinilah perhitungan faktorial yang sebernarnya dilakukan, disini menerima argumen n dan result, dimana n adalah jumalh faktorial yang dicari (seperti 5! atau 10!, etc), dan result adalah array result.
- memset tersebut digunakan untuk meng-set semua isi dari array menjadi 0, lalu isi array terakhir dirubah menjadi 1 (karena kita memulai perhitungan loop nya dari 2)

```c
    // calculate factorial
    for (int i = 2; i <= n; i++) {
        int carry = 0;
        for (int j = MAX_DIGITS - 1; j >= 0; j--) {
            int digit = (result[j] - '0') * i + carry;
            result[j] = digit % 10 + '0';
            carry = digit / 10;
        }
    }
```

- disinilah kita menghitung faktorial, untuk mempermudah penjelasan saya akan menggunakan visualisasi array dengan case 5!

1.  inisialisasi
    result [0, 0, 0 ,. ..., 1]

2.  iterasi 1, i = 2
    result [0, 0, 0, 0, .., 0, 2]
    carry = 0

3.  iterasi 2, i = 3
    result [0, 0, 0, ..., 0, 6]
    carry = 0

4.  iterasi 3, i = 4
    result [0, 0, 0, ..., 0, 2, 4]
    carry = 2

5.  iterasi 4, i = 5
    result [0, 0, 0, ..., 1, 2, 0]

dan seterusnya untuk faktorial yang lebih besar

```c
    // trim zeros
    int i = 0;
    while (result[i] == '0' && i < MAX_DIGITS - 1) {
        i++;
    }

    // Shift the digits to the beginning of the array.
    if (i > 0) {
        for (int j = i; j < MAX_DIGITS; j++) {
            result[j - i] = result[j];
        }
        memset(result + MAX_DIGITS - i, 0, i);
    }
```

- karena didalam array char tersebut masih terdapat banyak 0, maka potongan kode diatas adalah untuk menghilangkan 0 yang tidak berguna
- contoh [0, 0, 0, 0, 0, 0, 1, 2, 0], maka 0 yang tidak terpakai adalah hingga pada index ke 5
- sehingga 0 0 0 sebelum angka 1 akan dihilangkan, sehingga yang tersisa adalah [1, 2, 0]

```c
    // Print factorials
    printf("\nFactorials:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int n = *(shared_matrix + i * 5 + j);
            if (n > 0 && n <= 40) {
                printf("\n%d! = %s", n, factorials[i][j]);
            }
        }
    }
    printf("\n");

    shmdt(shared_matrix);

    clock_t end = clock();
    double time_spent = (double) (end - start) / CLOCKS_PER_SEC;

    printf("Total time taken: %f seconds\n", time_spent);
    return 0;
```

- pada bagian ini, print matrix mirip dengan code sebelumnya, hanya menggunakan loop biasa
- shmdt juga sama dengan code sebelumnya, yaitu untuk detach shared memory
- lalu ada perhitungan time_spent, dan print time_spent, ini untuk menunjukkan lama waktu yang diperlukan untuk program ini berjalan

### bagian d

### sisop.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#define MAX_DIGITS 140

void factorial(int n, char* result) {
    // initial result is set to 1
    memset(result, '0', MAX_DIGITS);
    result[MAX_DIGITS - 1] = '1';

    // calculate factorial (loop way)
    for (int i = 2; i <= n; i++) {

        int carry = 0;
        for (int j = MAX_DIGITS - 1; j >= 0; j--) {
            int digit = (result[j] - '0') * i + carry;
            result[j] = digit % 10 + '0';
            carry = digit / 10;
        }
    }

    //trim zeros
    int i = 0;
    while (result[i] == '0' && i < MAX_DIGITS - 1) {
        i++;
    }

    // Shift the digits to the beginning of the array.
    if (i > 0) {
        for (int j = i; j < MAX_DIGITS; j++) {
            result[j - i] = result[j];
        }
        memset(result + MAX_DIGITS - i, 0, i);
    }
}

int main(int argc, char const *argv[]) {
    // Start measuring time.
    clock_t start = clock();

    //key for shared memory
    key_t key = 1234;
    int shmid;
    int *shared_matrix;
    char factorials[4][5][MAX_DIGITS];

    //get shared_matrix from shared memory
    shmid = shmget(key, sizeof(int) * 20, 0666);
    shared_matrix = shmat(shmid, NULL, 0);

    // Calculate the factorial of each element in shared_matrix and store it in factorials.
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int n = *(shared_matrix + i * 4 + j);
            if (n > 0 && n <= 40) {
                factorial(n, factorials[i][j]);
            }
        }
    }

    // Print the factorials.
    printf("\nFactorials:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int n = *(shared_matrix + i * 5 + j);
            if (n > 0 && n <= 40) {
                printf("%d! = %s\n", n, factorials[i][j]);
            }
        }
    }

    printf("\n");

    shmdt(shared_matrix);

    clock_t end = clock();
    double time_spent = (double) (end - start) / CLOCKS_PER_SEC;

    printf("Total time taken: %f seconds\n", time_spent);
    return 0;
}
```

- pada bagian ini sama dengan bagian sebelumnya, kecuali disini tidak menggunakan multithreading, jadi kita langsung melakukan perhitungan faktorial pada

```c
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int n = *(shared_matrix + i * 4 + j);
            if (n > 0 && n <= 40) {
                factorial(n, factorials[i][j]);
            }
        }
    }
```

- yang kemudian di print kelayar juga bersama dengan jumlah waktu eksekusi


### OUTPUT 

### Kalian.c

![image](/uploads/7b58dfcee1af09494d52b195cf42121d/image.png)

- dapat dilihat isi matrix 1 dan matrix 2 lalu hasil perkaliannya

### Cinta.c

![image](/uploads/e9440d5d53eb7971fe75b6628be77240/image.png)


### Sisop.c

![image](/uploads/5f32ad8c6aaa8c29f2fbee1303d000ad/image.png)


- setelah dilihat, hasil dari penggunaan thread pada cinta.c, tidak membuat jalannya program lebih cepat dari metode biasa. disini penggunaan loop biasa membuat program berjalan lebih cepat. 


## Soal 3

### Bagian A dan B

Pertama membuat 2 file yaitu stream.c sebagai reciever dan user.c sebagai sender yang berinteraksi interproses menggunakan message queue. Pertama membuat message queue di kedua file tersebut

#### USER (SENDER)

```c
    key_t key;
    int msgid;
    int len, retval;

    key = ftok("progfile", 65);
    msgid = msgget(key, 0666 | IPC_CREAT);
    message.mesg_type = 1;
    message.mesg_uid = getpid();

    while(fgets(message.mesg_text,MAX,stdin) != NULL){
        len = strlen(message.mesg_text);
        if(message.mesg_text[len-1] == '\n')
            message.mesg_text[len-1] = '\0';

        msgsnd(msgid, &message, (int)sizeof(message), 0);
    }
    //end
    strcpy(message.mesg_text, "end");
    len = strlen(message.mesg_text);
    if(message.mesg_text[len-1] == '\n')
        message.mesg_text[len-1] = '0';
    msgsnd(msgid, &message, len+1, 0);
```

#### STREAM (RECIEVER)

```c
    key_t key;
    int msgid;
    int toEnd;

    key = ftok("progfile", 65);
    msgid = msgget(key, 0666 | IPC_CREAT);

    msgrcv(msgid, &message, sizeof(message), 0, 0);
    msgctl(msgid, IPC_RMID, NULL);
```

Untuk membaca file.json yaitu menggunakan user defined library cJSON.h

````c
        FILE *f = fopen("song-playlist.json", "r");
        FILE *ft = fopen("playlist.txt", "a");
        if (!f && !ft)
        {
            fprintf(stderr, "error open file");
        }
        // size dari file
        fseek(f, 0, SEEK_END);
        long file_size = ftell(f);
        rewind(f);

        // baca file
        char *buffer = (char *)malloc(file_size + 1);
        fread(buffer, 1, file_size, f);
        buffer[file_size] = '\0';
        fclose(f);

        // menguraikan json
        cJSON *data = cJSON_Parse(buffer);

        // menampilkan data
        if (cJSON_IsArray(data))
        {
            cJSON *item = NULL;
            cJSON_ArrayForEach(item, data)
            {
                cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
                cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");
                if (cJSON_IsString(method) && (method->valuestring != NULL))
                {
                    ```
                    DECODE HERE
                    ```
                }
            }
        }
        fclose(ft);
        fclose(f);
        cJSON_Delete(data);
        free(buffer);
        exit(1);

````

Untuk melakukan decrypt sesuai dengan metode-nya yaitu menggunakan fungsi berikut :

#### ROT13

```c
char *decrypt_rot13(char *cipher)
{
    char *plaintext;
    int i, shift = 13;
    for (i = 0; cipher[i] != '\0'; i++)
    {
        if (isalpha(cipher[i]))
        {
            if (isupper(cipher[i]))
            {
                plaintext[i] = ((cipher[i] - 'A') + shift) % 26 + 'A';
            }
            else
            {
                plaintext[i] = ((cipher[i] - 'a') + shift) % 26 + 'a';
            }
        }
        else
        {
            plaintext[i] = cipher[i];
        }
    }
    plaintext[i] = '\0';

    return plaintext;
}
```

#### Base64

```c
const char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int base64_decode(char *input, int input_len, char *output, int output_len)
{
    int i, j, k;
    unsigned char a, b, c, d;

    for (i = 0, j = 0; i < input_len && j < output_len; i += 4, j += 3)
    {
        a = input[i];
        b = input[i + 1];
        c = input[i + 2];
        d = input[i + 3];

        a = strchr(base64_chars, a) - base64_chars;
        b = strchr(base64_chars, b) - base64_chars;
        c = (c == '=') ? 0 : strchr(base64_chars, c) - base64_chars;
        d = (d == '=') ? 0 : strchr(base64_chars, d) - base64_chars;

        output[j] = (a << 2) | (b >> 4);
        if (c != 0xFF)
        {
            output[j + 1] = (b << 4) | (c >> 2);
            if (d != 0xFF)
            {
                output[j + 2] = (c << 6) | d;
            }
        }
    }

    return j;
}

char *decrypt_base64(char *base64)
{
    char *plaintext;
    int len;

    len = strlen(base64);
    if (base64[len - 1] == '\n')
    {
        base64[len - 1] = '\0';
        len--;
    }

    // Decode Base64 string to plaintext
    int plaintext_len = base64_decode(base64, len, plaintext, 1000);
    plaintext[plaintext_len] = '\0';

    return plaintext;
}
```

#### HEX

```c
void decrypt_hex(const char *hex_string, char *output_string)
{
    int i, length;

    // hitung panjang string hex
    length = strlen(hex_string);

    // pastikan panjang string hex adalah bilangan genap
    if (length % 2 != 0)
    {
        fprintf(stderr, "Panjang string hex harus genap\n");
        return;
    }

    // konversi string hex menjadi string biasa
    for (i = 0; i < length; i += 2)
    {
        char buffer[3];
        buffer[0] = hex_string[i];
        buffer[1] = hex_string[i + 1];
        buffer[2] = '\0';

        output_string[i / 2] = (char)strtol(buffer, NULL, 16);
    }

    output_string[i / 2] = '\0';
}
```

Setelah dilakukan decrypt dari file song-playlist.json kemudian diurutkan menurut alfabet dan dioutputkan menjadi playlist.txt yaitu sebagai berikut :

```c
system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
system("rm playlist.txt | mv sorted.txt playlist.txt");
```

agar decrypt dan pengurutan-nya dapat berjalan dengan berhasil, maka dilakukan fork. Child process untuk melakukan decrypt dan parent process akan menunggu child process selesai untuk melakukan pengurutan line dan meng-output playlist.txt

### Bagian C, D, E, dan G

Untuk menerima perintah dari user maka stream harus dapat membaca perintah yang sesuai yaitu dengan cara sebagai berikut :

```c
            if (strcmp(message.msg_text, "DECRYPT") == 0)
            {
                printf("Decrypt\n");
                Decrypt();
            }
            else if (strcmp(message.msg_text, "LIST") == 0)
            {
                system("cat playlist.txt");
            }
            else if (strstr(message.msg_text, "PLAY") != NULL)
            {
                Play();
            }
            else if (strstr(message.msg_text, "ADD") != NULL)
            {
                Add();
            }
            else
            {
                printf("UNKNOWN COMMAND\n");
            }
            toEnd = strcmp(message.msg_text, "end");
            if (toEnd == 0)
                break;
```

Untuk melakukan perintah LIST yaitu sebagai berikut :

```c
system("cat playlist.txt");
```

Dengan menggunakan system cat untuk menampilkan isi dari playlist.txt

Untuk melakukan perintah PLAY <SONG> yaitu sebagai berikut :

```c
void Play()
{
    char *cmd = "awk -F ' - ' 'tolower($1) ~ tolower(\"%s\") {print$%d}' playlist.txt";
    char buffer[50], song[50], singer[50][100], play[50][100];
    int i = 0, j = 5, k = 2;
    int index_singer = 0, index_play = 0;

    while (j != strlen(message.msg_text) + 1)
    {
        song[i] = message.msg_text[j];
        i++;
        j++;
    }
    song[i] = '\0';
    char song2[50];
    strcpy(song2, song);
    if (strlen(song) > 0)
    {
        sprintf(buffer, cmd, song, k--);
        FILE *fp_singer = popen(buffer, "r");
        if (fp_singer == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        // get singer
        while (fgets(singer[index_singer], sizeof(singer[index_singer]), fp_singer) != NULL)
            index_singer++;

        sprintf(buffer, cmd, song2, k);
        FILE *fp_play = popen(buffer, "r");
        if (fp_play == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        while (fgets(play[index_play], sizeof(play[index_play]), fp_play) != NULL)
            index_play++;
        fclose(fp_play);
        fclose(fp_singer);

        if (index_singer == 1)
        {
            // delete new line in singer
            int len_singer = strlen(singer[index_singer - 1]);
            if (singer[index_singer - 1][len_singer - 1] == '\n')
            {
                singer[index_singer - 1][len_singer - 1] = '\0';
            }
            // delete new line in play
            int len_play = strlen(play[index_singer - 1]);
            if (play[index_singer - 1][len_play - 1] == '\n')
            {
                play[index_singer - 1][len_play - 1] = '\0';
            }
            printf("USER <%d> PLAYING \"%s - %s\"\n", message.msg_uid, singer[index_singer - 1], play[index_singer - 1]);
        }
        else if (index_singer > 1)
        {
            printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", index_singer, song2);
            int index_list = 1;
            while (index_singer > 0)
            {
                // delete new line in singer
                int len_singer = strlen(singer[index_singer - 1]);
                if (singer[index_singer - 1][len_singer - 1] == '\n')
                {
                    singer[index_singer - 1][len_singer - 1] = '\0';
                }
                // delete new line in play
                int len_play = strlen(play[index_singer - 1]);
                if (play[index_singer - 1][len_play - 1] == '\n')
                {
                    play[index_singer - 1][len_play - 1] = '\0';
                }
                printf("%d. %s - %s\n", index_list++, singer[index_singer - 1], play[index_singer - 1]);
                index_singer--;
            }
        }
        else
        {
            printf("THERE IS NO SONG CONTAINING %s\n", song2);
        }
    }
    else
    {
        printf("UNKNOWN COMMAND\n");
    }
}
```

Menerima message queue dan menghapus karakter terakhir yang berupa new line menjadi '\0' untuk mengakhiri string. Cek string setelah kata PLAY untuk mencari nama lagunya. Kemudian menjalankan system untuk memisahkan antara penyanyi dan lagu kedalam array untuk di print di console. Juga dilengkapi handle untuk lagu yang tidak ada di playlist.txt dan handle untuk UNKNOWN COMMAND apabila tidak menuliskan string apapun setelah PLAY

Untuk melakukan perintah ADD <SONG> yaitu sebagai berikut :

```c
void Add()
{
    char *cmd = "awk 'tolower($0) ~ tolower(\"%s\") {print$0}' playlist.txt";
    int j = 4, i = 0;
    char song[50], buffer[50];

    while (j != strlen(message.msg_text) + 1)
    {
        song[i] = message.msg_text[j];
        i++;
        j++;
    }
    song[i] = '\0';
    if (strlen(song) > 0)
    {
        char playlist[100];
        sprintf(buffer, cmd, song);
        FILE *fp_song = popen(buffer, "r");
        if (fp_song == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        // find song
        pid_t pid;
        int status;

        // find song in play list
        fgets(playlist, sizeof(playlist), fp_song);

        // assign song in playlist
        if (strstr(playlist, song) != NULL)
        {
            printf("SONG ALREADY ON PLAYLIST\n");
        }
        else if ((song[0] != '\0') && strstr(playlist, song) == NULL)
        {
            pid = fork();
            if (pid == 0)
            {
                cmd = "echo %s >> playlist.txt";
                sprintf(buffer, cmd, song);
                system(buffer);
                exit(1);
            }
            wait(&status);

            system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
            system("rm playlist.txt | mv sorted.txt playlist.txt");

            printf("USER <%d> ADD %s\n", message.msg_uid, song);
        }
        else
        {
            printf("UNKNOWN COMMAND\n");
        }
    }
    else
    {
        printf("UNKNOWN COMMAND\n");
    }
}
```

Perlu untuk mencari lagu didalam playlist.txt dengan menjalankan system diatas. Apabila lagu yang akan di ADD tidak ada diplaylist maka dilakukan system echo dan ditulis di playlist.txt. Hal ini dilakukan dengan fork() untuk menunggu perangkat menulis di playlist.txt selesai kemudian dilakukan pengurutan kembali menggunakan system dan terakhir akan diprint di console. Dilengkapi handle apabila setelah kata ADD tidak menuliskan string apapun.

### Bagian F

Menggunakan semaphore untuk membatasi user yang akan mengakses playlist yaitu sebagai berikut :

#### USER (SENDER)

````c
    sem_t *semaphore_rsc = sem_open(SEM_NAME, 0);
    if(semaphore_rsc == SEM_FAILED){
		perror("semopen ");
		exit(1);
	}

    ```
    CODE HERE
    ```

    sem_post(semaphore_rsc);
	sem_close(semaphore_rsc);
````

#### STREAM (RECIEVER)

````c
    sem_t *semaphore_rsc = sem_open(SEM_NAME, O_CREAT, 0644, MAX_USER);
    int semVal, retrv, userID[2], index_user = 0;

        if(index_user <= 2 && userID[0] != message.msg_uid){
            userID[index_user] = message.msg_uid;
            index_user++;
        }else{
            if(userID[0] != message.msg_uid && userID[1] != message.msg_uid){
                printf("STREAM SYSTEM OVERLOAD\n");
                continue;
            }
        }

        if (sem_getvalue(semaphore_rsc, &semVal) == 0)
        {
            ```
            BAGIAN B, C, D, E, DAN G
            ```
        }

        sem_close(semaphore_rsc);
        sem_unlink(SEM_NAME);
````

Pada STREAM semaphore akan dibuat terlebih dahulu agar dapat dibuka oleh USER dengan nama semaphore yang sama. Untuk membatasi user maka perlu ditulis diakhir parameter semaphore yaitu MAX_USER yang sudah didefinisikan sebelumnya.

### MAIN FUNCTION

#### USER

```c
int main(){
	key_t key;
	int msgid;
    int len, retval;
	sem_t *semaphore_rsc = sem_open(SEM_NAME, 0);
	if(semaphore_rsc == SEM_FAILED){
		perror("semopen ");
		exit(1);
	}
	key = ftok("progfile", 65);

	msgid = msgget(key, 0666 | IPC_CREAT);
	message.mesg_type = 1;
	message.mesg_uid = getpid();

	printf("Ready to send messages.\n");
    printf("Enter lines of text, ^D to quit\n");
	while(fgets(message.mesg_text,MAX,stdin) != NULL){
        len = strlen(message.mesg_text);
        if(message.mesg_text[len-1] == '\n')
            message.mesg_text[len-1] = '\0';

		msgsnd(msgid, &message, (int)sizeof(message), 0);
    }
    //end
    strcpy(message.mesg_text, "end");
    len = strlen(message.mesg_text);
    if(message.mesg_text[len-1] == '\n')
        message.mesg_text[len-1] = '0';
	msgsnd(msgid, &message, len+1, 0);

	// display the message
	printf("Done sending messages.\n");
	sem_post(semaphore_rsc);
	sem_close(semaphore_rsc);

	return 0;
}
```

Pada main function USER akan dijalankan semaphore yang sudah dijelaskan sebelumnya dengan meneriman input dari user sampai kata end di inputkan di console untuk menutup USER / dengan ^D. Sebelum process berhenti maka yang perlu dilakukan yaitu sem_post untuk menambahkan nilai semaphore yang sebelumnya dikurangi saat process ini dijalankan, kemudia sem_close untuk menutup semaphore.

#### STREAM

```c
int main()
{
    key_t key;
    int msgid;
    int toEnd;
    sem_t *semaphore_rsc = sem_open(SEM_NAME, O_CREAT, 0644, MAX_USER);

    key = ftok("progfile", 65);
    msgid = msgget(key, 0666 | IPC_CREAT);

    printf("Ready to receive messages\n");
    int semVal, retrv, userID[2], index_user = 0;
    for (;;)
    {
        msgrcv(msgid, &message, sizeof(message), 0, 0);
        if(index_user <= 2 && userID[0] != message.msg_uid){
            userID[index_user] = message.msg_uid;
            index_user++;
        }else{
            if(userID[0] != message.msg_uid && userID[1] != message.msg_uid){
                printf("STREAM SYSTEM OVERLOAD\n");
                continue;
            }
        }

        retrv = sem_getvalue(semaphore_rsc, &semVal);

        if (sem_getvalue(semaphore_rsc, &semVal) == 0)
        {
            if (strcmp(message.msg_text, "DECRYPT") == 0)
            {
                printf("Decrypt\n");
                Decrypt();
            }
            else if (strcmp(message.msg_text, "LIST") == 0)
            {
                system("cat playlist.txt");
            }
            else if (strstr(message.msg_text, "PLAY") != NULL)
            {
                Play();
            }
            else if (strstr(message.msg_text, "ADD") != NULL)
            {
                Add();
            }
            else
            {
                printf("UNKNOWN COMMAND\n");
            }
            toEnd = strcmp(message.msg_text, "end");
            if (toEnd == 0)
                break;
        }
    }
    sem_close(semaphore_rsc);
    msgctl(msgid, IPC_RMID, NULL);
    sem_unlink(SEM_NAME);
    printf("Done receiving messages.\n");
    return 0;
}
```

Pada main function STREAM membuat semaphore dengan nama yang sudah didefinisikan sebelumnya yang sama dengan nama semaphore pada USER. STREAM akan berjalan terus sampai USER passing string end / ^D pada message queue yang dibaca oleh STREAM. Untuk menampilkan STREAM OVERLOAD apabila USER lebih dari yang ditentukan, maka diperlukan mencatat pid dari USER kedalam array yang ada dan mengeceknya apabila USER tidak memiliki pid yang terdaftar di array. Sebelum process STREAM berakhir, diperlukan untuk menutup semaphore dan menghapus message queue yang sudah dibuat pada process STREAM dan USER. Juga dilakukan sem_unlink untuk menghapus koneksi antara process ini dan semaphore yang dibuat sebelumnya dan semaphore akan dihapus apabila tidak digunakan lagi.

Output :
![Screenshot_from_2023-05-11_08-40-39](/uploads/920a9cea530185e4179216303ff84a95/Screenshot_from_2023-05-11_08-40-39.png)

![Screenshot_from_2023-05-11_08-42-31](/uploads/8652b1130374c92d0ed4877fffd26054/Screenshot_from_2023-05-11_08-42-31.png)

![Screenshot_from_2023-05-11_08-42-51](/uploads/770d9e4b1a794d04e4b9209796976a8e/Screenshot_from_2023-05-11_08-42-51.png)

![Screenshot_from_2023-05-11_08-44-30](/uploads/beae93a6223f27c20216dac1905ff8dd/Screenshot_from_2023-05-11_08-44-30.png)

![Screenshot_from_2023-05-11_08-44-42](/uploads/bbbf721499d2c20d91542bec6a3bf0b9/Screenshot_from_2023-05-11_08-44-42.png)

![Screenshot_from_2023-05-11_08-45-49](/uploads/8ba938a19895a0183df72ad429807116/Screenshot_from_2023-05-11_08-45-49.png)

![Screenshot_from_2023-05-11_08-46-03](/uploads/1c4fb724553405ff1406ea1e04c6bf68/Screenshot_from_2023-05-11_08-46-03.png)

![Screenshot_from_2023-05-11_08-46-12](/uploads/d4dec5b2eb993fa3a27e9aa7a691efd3/Screenshot_from_2023-05-11_08-46-12.png)

![Screenshot_from_2023-05-11_08-46-41](/uploads/acc6741c1a5e4d4f08905a22d3cb123a/Screenshot_from_2023-05-11_08-46-41.png)

![Screenshot_from_2023-05-11_09-12-07](/uploads/fc2457f939f6081f428ba88de9445067/Screenshot_from_2023-05-11_09-12-07.png)

## Soal 4

Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut!

- Download dan unzip file tersebut dalam kode c bernama unzip.c.
- Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
  Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
- Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
  - extension_a : banyak_file
  - extension_b : banyak_file
  - extension_c : banyak_file
  - other : banyak_file
- Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
- Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.

  - DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
  - DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
  - DD-MM-YYYY HH:MM:SS MADE [folder name]

- examples :

  - 02-05-2023 10:01:02 ACCESSED files
  - 02-05-2023 10:01:03 ACCESSED files/abcd
  - 02-05-2023 10:01:04 MADE categorized
  - 02-05-2023 10:01:05 MADE categorized/jpg
  - 02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg

- Catatan:
  - Path dimulai dari folder files atau categorized
  - Simpan di dalam log.txt
  - ACCESSED merupakan folder files beserta dalamnya
  - Urutan log tidak harus sama
- Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
  - Untuk menghitung banyaknya ACCESSED yang dilakukan.
  - Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
  - Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

Penyelesaian dan penjelasan :
untuk unzip file yang terletak pada `unzip.c` kita akan mendaur ulang kode pada modul 2 untuk unzip menggunakan multiprocess

```c
    char file_url[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char nama_file[] = "hehe.zip";

    child_id = fork();
    if(child_id == 0) {
        execlp("wget", "wget", "-O", nama_file, file_url, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("unzip", "unzip", nama_file, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("rm", "rm", nama_file, NULL);
    }

    printf("Done Donwloading and Unzipping file\n");

```

seperti pada umumnya, kita download terlebih dauhulu file zip nya dengan `wget` lalu kita unzip dan file zip nya kita hapus untuk menghemat ruang

selanjutnya kita akan beralih ke `categorize.c`, untuk mengkategorikanya perlu beberapa tahap, dapat dilihat pada kode berikut

```c
int main(){

    pthread_create(&threads[0], NULL, listfile, NULL);
    pthread_join(threads[0],NULL);

    pthread_create(&threads[0], NULL, createdir, NULL);
    pthread_join(threads[0],NULL);

    pthread_create(&threads[0], NULL, movefile, NULL);
    pthread_join(threads[0],NULL);

    pthread_create(&threads[0], NULL, count_eks, NULL);
    pthread_join(threads[0],NULL);

    system("rm listFiles.txt");
    system("rm -r files");

    return 0;
}

```

untuk setiap tahap kita akan menggunakan multi thread dengan membuat thread baru. yang pertama dilakukan adalah listfile pada folder `./files` dan juga create directory untuk setiap ekstension (sebenarnya kedua thread `createdir` dan `listfiles` dapaat dijalankan bersamaan karena tidak berhubungan, namun sepertinya karena keterbatasan thread proses listfile yang cukup berat sehingga tidak dapat dijalankan bersamaan)

```c
void listdir(const char *name)
{
    DIR *dir;
    struct dirent *entry;
    char cmd[MAX_LINE_LENGTH];

    if (!(dir = opendir(name)))
        return;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            char path[1024];
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            create_thread_logging(1, path, "null", "null");
            listdir(path);
        } else {
            sprintf(cmd, "echo '%s/%s' >> listFiles.txt", name,entry->d_name);
            system(cmd);
        }
    }
    closedir(dir);
}


void *listfile(){
    char charMax [3];
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "max.txt");
    textfile = fopen (nama_file, "r"); fgets (charMax, 3, textfile); fclose(textfile);
    max = atoi(charMax);

    listdir("./files");

}
```

untuk thread `listfile` kita perlu membaca `max.txt` dan menyimpan ke variabel untuk mengetahui max file tiap folder, lalu kita perlu fungsi rekursif untuk mentraverse semua subdirectory dan files pada `./files` lalu menyimpanya pada `listFiles.txt` untuk sementara. Perlu diperhatikan juga setiap mengakses directory, akan memanggil fungsi untuk `create_thread_logging` yang nanti akan dijelaskan lebih lanjut.

```c
void *createdir(){
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(folder_name, "./categorized");
    sprintf(cmd, "mkdir -p %s", folder_name);
    system(cmd);

    create_thread_logging( 3, folder_name, "null", "null");

    strcpy(nama_file, "extensions.txt");
    int index=0;
    textfile = fopen(nama_file, "r");

    create_thread_logging( 1, nama_file , "null", "null");

    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-2] = '\0';
        sprintf(cmd, "mkdir -p %s/%s", folder_name, line);
        strcpy(ekstension[index], line);
        ekstensionDir[index] = 0;
        index++;
        extCount++;
        system(cmd);

        sprintf(buffer, "%s/%s", folder_name, line);
        create_thread_logging(3, buffer, "null", "null");
    }
    fclose(textfile);
    sprintf(cmd, "mkdir -p %s/other", folder_name);
    system(cmd);

    sprintf(buffer, "%s/other", folder_name);
    create_thread_logging( 3, buffer, "null", "null");
}
```

lalu untuk thread `createdir`, akan membaca file `extension.txt` lalu membuat directory `./categorized` dan dir untuk setiap ekstension di dalamnya selain itu kita juga harus membuat array ektensi dan jumlah nya untuk memudahkan perhitungan. sama seperti sebelumnya setiap membuat directory, akan memanggil fungsi `create_thread_logging` yang akan dijelaskan lebih lanjut

setelah menyelesaikan kedua thread tersebut, barulah kita bisa memindah mindahkan files sesuai dengan tempatnya masing masing menggunakan thread `movefile`

```c
const char *get_filename_ext(char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

int check_ekstension(char eks[]){
    for(int i=0; i<extCount;i++){
        if(strcmp(eks, ekstension[i]) == 0)
            return i;
    }
    return -1;
}

void *movefile(){
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "listFiles.txt");
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-1] = '\0';
        char eks[MAX_LINE_LENGTH];
        sprintf(eks,"%s",get_filename_ext(line));
        for(int i = 0; eks[i]; i++){
            eks[i] = tolower(eks[i]);
        }
        if(check_ekstension(eks) == -1){

            sprintf(cmd, "cp '%s' ./categorized/other", line);
            system(cmd);
            create_thread_logging(2, "other", line,"./categorized/other" );
        }
        else{
            if(ekstensionDir[check_ekstension(eks)] < max){
                sprintf(cmd, "cp '%s' ./categorized/%s", line, eks);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s", eks);
                create_thread_logging(2, eks, line, buffer );
            }else{
                int num = ekstensionDir[check_ekstension(eks)] / max + 1;
                sprintf(buffer, "./categorized/%s %d", eks, num);
                if(access(buffer, F_OK) != 0){
                    create_thread_logging(3,buffer, "null", "null" );
                }
                sprintf(cmd, "mkdir -p './categorized/%s %d'", eks, num);
                system(cmd);

                sprintf(cmd, "cp '%s' './categorized/%s %d'", line, eks, num);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s %d", eks, num);
                create_thread_logging(2, eks, line, buffer );
            }
        }
    }
}
```

untuk memindahkan filesnya, kita hanya perlu membaca `listfile.txt` yang berisi semua path file yang ada pada dire `./files`. tapi setiap kali kita memindahkannya kita harus menambahkan jumlah file pada array ekstensionm yang akan digunakan untuk mengetahui letak destinasi files, jika folder ekstensi sudah melebihi nilai maksimalnya, maka akan dibuat folder baru dengan tambahan angka dibelakangnya, begitu seterusnya hingga setiap path file sudah dipindahkan ke directory `./categorized`. sama seperti sebelumnya, setiap kali memindahkan files atau membuat directory kita perlu memanggil fungsi `create_thread_logging`.

perintah selanjutnya dalam `categorize.c` adalah membuat logging yang akan disimpan pada `log.txt` sesuai perintah pada Soal

```c
int mode = 1;
char param1[MAX_LINE_LENGTH];
char param2[MAX_LINE_LENGTH];
char param3[MAX_LINE_LENGTH];

void *logging(){

    // mode : 1 = acces, 2 = move, 3 = made
    char file_log[] = "log.txt";

    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    if(mode == 1){
        char mode[] = "ACCESSED";
        char write[MAX_LINE_LENGTH];
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
    }else if(mode == 2){
        char mode[] = "MOVED";
        char write[MAX_LINE_LENGTH];
        sprintf(write, "%s %s %s file : %s > %s", datetime, mode, param1, param2, param3);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
    }else{
        char mode[] = "MADE";
        char write[MAX_LINE_LENGTH];
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
    }
}

void create_thread_logging(int modeNew, char param1New[], char param2New[], char param3New[]){
    mode = modeNew;
    strcpy(param1, param1New);
    strcpy(param2, param2New);
    strcpy(param3, param3New);

    pthread_join(threads[1], NULL);
    pthread_create(&threads[1], NULL, logging, NULL);
    pthread_join(threads[1], NULL);
}
```

fungsi `create_thread_logging` akan membuat thread untuk menjalankan fungsi logging setiap kali dipanggil. thread logging akan menggunakan time saat thread dipanggil dan juga mode `MOVED | ACCESSED | MADE` untuk mengetahui apa yang akan ditulis ke `log.txt`, untuk setiap mode, parameter yang digunakan berbeda, sesuai dengan format yang diberikan. setiap kali dipanggil, thread akan mengappend file `log.txt` sesuai dengan proses yang sedang dilakukan.

terakhir dari `categorize.c` adalah menampilkan list ekstensi dan jumlah filesnya

```c
void *count_eks(){
    strcpy(ekstension[extCount], "other");
    DIR *directory;
    struct dirent *entry;
    directory = opendir("./categorized/other");

    create_thread_logging(1, "./categorized/other", "null", "null");

    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_REG) {
            ekstensionDir[extCount]++;
        }
    }

    closedir(directory);
    extCount++;

    for(int i=0;i<extCount; i++){
        for(int j=0; j<extCount-1;j++){
            if(ekstensionDir[j] > ekstensionDir[j+1]){
                int temp;
                temp = ekstensionDir[j];
                ekstensionDir[j] = ekstensionDir[j+1];
                ekstensionDir[j+1] = temp;

                strcpy(buffer, ekstension[j]);
                strcpy(ekstension[j], ekstension[j+1]);
                strcpy(ekstension[j+1], buffer);

            }
        }
    }

    for(int i=0; i<extCount; i++){
        printf("%s : %d\n", ekstension[i], ekstensionDir[i]);
    }
}
```

cukup simpel, karna kita sudah menyimpan ekstensi dan jumlahnya pada array, kita hanya perlu menambahkan ektensi other pada array lalu menghitung files yang ada pada ekstensi other. setelah itu kita akan mengurutkanya ascending berdasarkan jumlah filenya dan mencetaknya ke console

Perintah sleanjutnya akan dilakukan pada `logchecker.txt`, yang perttama adalah menghitung accessed folder pada `log.txt`

```c
int count_accessed(){
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];
    int counter = 0;
    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "ACCESSED") != NULL) {
            counter++;
        }

    }
    fclose(textfile);
    return counter;
}
```

kita hanya perlu mengiterasi setiap line pada `log.txt` jika menemukan `ACCESSED` maka kita akan menambah counter, jika fie EOF, maka return nilai counteruk di print

yang kedua eadalah menampilkan setiap directory yang ada dan menampilkan jumlah files pada tiap directory

```c
char dir[MAX_LINE_LENGTH][MAX_LINE_LENGTH];
int dirCount[MAX_LINE_LENGTH];
int indexDir = 0;

int check_dir(char dirs[] ){
    for(int i=0; i<indexDir;i++){
        if(strcmp(dirs, dir[i]) == 0)
            return i;
    }
    return -1;
}

void all_dir_count(){

    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "MADE ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *pos = strstr(line, "MADE ");
            char *file = pos + strlen("MADE ");

            strcpy(dir[indexDir], file);
            dirCount[indexDir] = 0;
            indexDir++;
        }
    }
    fclose(textfile);

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "> ") != NULL) {
                line[strlen(line)-1] = '\0';
                char *pos = strstr(line, "> ");
                char *file = pos + strlen("> ");

                int num = check_dir(file);
                dirCount[num]++;
        }
    }
    fclose(textfile);


    for(int i=0;i<indexDir; i++){
        for(int j=0; j<indexDir-1;j++){
            if(dirCount[j] > dirCount[j+1]){
                int temp;
                temp = dirCount[j];
                dirCount[j] = dirCount[j+1];
                dirCount[j+1] = temp;

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, dir[j]);
                strcpy(dir[j], dir[j+1]);
                strcpy(dir[j+1], buffer);

            }
        }
    }

    for(int i=0; i<indexDir; i++){
        printf("%s : %d\n", dir[i], dirCount[i]);
    }

}
```

kita perlu membuat array untuk menyimpan sudirectory yang ada dan jumlah filesnya, ktia perlu mengiterasi line dri `log.txt`, jika menemukan `MADE` maka itu berarti membuat directory, kita akan menambahkan directory nya ke array. lalu kita sekali lagi mengiterasi file `log.txt` untuk menghitung jumlah `MOVED` pada setiap directory. selanjutnya kita urutkan array berdasar jumlah files dan kita tampilkan pada console.

yang terakhir adalah meglist tiap ekstensi dan jumlahnya,

```c
void all_eks_count(){
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    // strcpy(nama_file, "log.txt");
    // textfile = fopen(nama_file, "r");
    // while(fgets(line, MAX_LINE_LENGTH, textfile)){
    //     if (strstr(line, "MOVED ") != NULL) {
    //         line[strlen(line)-1] = '\0';
    //         char *start = strstr(line, "MOVED ");
    //         char *en = strstr(start, " file :");
    //         char file[MAX_LINE_LENGTH];
    //         strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED "));
    //         file[en - start - strlen("MOVED ")] = '\0';

    //         if(check_eks(file) == -1)
    //         {
    //             strcpy(eks[indexEks], file);
    //             eksCount[indexEks] = 0;
    //             indexEks++;
    //         }
    //     }
    // }
    // fclose(textfile);

    strcpy(nama_file, "extensions.txt");
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-2] = '\0';
        strcpy(eks[indexEks], line);
        eksCount[indexEks] = 0;
        indexEks++;
    }
    fclose(textfile);
    strcpy(eks[indexEks], "other");
    eksCount[indexEks]=0;
    indexEks++;

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "MOVED ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *start = strstr(line, "MOVED ");
            char *en = strstr(start, " file :");
            char file[MAX_LINE_LENGTH];
            strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED "));
            file[en - start - strlen("MOVED ")] = '\0';

            int num = check_eks(file);
            eksCount[num]++;
        }
    }
    fclose(textfile);


    for(int i=0;i<indexEks; i++){
        for(int j=0; j<indexEks-1;j++){
            if(eksCount[j] > eksCount[j+1]){
                int temp;
                temp = eksCount[j];
                eksCount[j] = eksCount[j+1];
                eksCount[j+1] = temp;

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, eks[j]);
                strcpy(eks[j], eks[j+1]);
                strcpy(eks[j+1], buffer);

            }
        }
    }

    for(int i=0; i<indexEks; i++){
        printf("%s : %d\n", eks[i], eksCount[i]);
    }
}
```

ada dua cara mengetahui ektensi, yang pertama dengan membaca file `ekstension.txt`, atau dengan cara membaca setiap ekstensi yang muncul pada `log.txt`, untuk cara kedua, ekstensi yang ada pada file .txt tapi tidak pernah dipindakan, tidak akan tercatat. sama seperti sebelum sebelumnya, ktia hanya perlu mengiterasi line `log.txt`, jika bertemu dengan `MOVED` kita cari tau ekstensinya dan kita tambahkan nilai count untuk array ekstensi tersebut, lalu kita urutkan berdasarkan banyak file, dan kita cetak ke console.

Output :
![image](/uploads/e0546c61ebd0d3dfb1fad3df64698c92/image.png)

![image](/uploads/567b8e195ceec1c587f79eec36a67858/image.png)

![image](/uploads/5b268a73ee1c0f7b3649e8aa6c84a8dc/image.png)
