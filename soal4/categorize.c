#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>

#define MAX_LINE_LENGTH 300

int extCount = 0;
char ekstension[MAX_LINE_LENGTH][10];
int ekstensionDir[MAX_LINE_LENGTH];
char buffer[MAX_LINE_LENGTH];
int max = 0;
pthread_t threads[2];

int mode = 1;
char param1[MAX_LINE_LENGTH];
char param2[MAX_LINE_LENGTH];
char param3[MAX_LINE_LENGTH];

void *logging(){

    // mode : 1 = acces, 2 = move, 3 = made
    char file_log[] = "log.txt";

    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    if(mode == 1){
        char mode[] = "ACCESSED";
        char write[MAX_LINE_LENGTH];
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
    }else if(mode == 2){
        char mode[] = "MOVED";
        char write[MAX_LINE_LENGTH];
        sprintf(write, "%s %s %s file : %s > %s", datetime, mode, param1, param2, param3);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
    }else{
        char mode[] = "MADE";
        char write[MAX_LINE_LENGTH];
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
    }
}

void create_thread_logging(int modeNew, char param1New[], char param2New[], char param3New[]){
    mode = modeNew;
    strcpy(param1, param1New);
    strcpy(param2, param2New);
    strcpy(param3, param3New);

    pthread_join(threads[1], NULL);
    pthread_create(&threads[1], NULL, logging, NULL);
    pthread_join(threads[1], NULL);
}

const char *get_filename_ext(char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

int check_ekstension(char eks[]){
    for(int i=0; i<extCount;i++){
        if(strcmp(eks, ekstension[i]) == 0)
            return i;
    }
    return -1;
}

void *createdir(){
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(folder_name, "./categorized");
    sprintf(cmd, "mkdir -p %s", folder_name);
    system(cmd);

    create_thread_logging( 3, folder_name, "null", "null");

    strcpy(nama_file, "extensions.txt");
    int index=0;
    textfile = fopen(nama_file, "r");

    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-2] = '\0';
        sprintf(cmd, "mkdir -p %s/%s", folder_name, line);
        strcpy(ekstension[index], line);
        ekstensionDir[index] = 0;
        index++;
        extCount++;
        system(cmd);

        sprintf(buffer, "%s/%s", folder_name, line);
        create_thread_logging(3, buffer, "null", "null");
    }
    fclose(textfile);
    sprintf(cmd, "mkdir -p %s/other", folder_name);
    system(cmd);

    sprintf(buffer, "%s/other", folder_name);
    create_thread_logging( 3, buffer, "null", "null");
}

void listdir(const char *name)
{
    DIR *dir;
    struct dirent *entry;
    char cmd[MAX_LINE_LENGTH];

    if (!(dir = opendir(name)))
        return;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            char path[1024];
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            create_thread_logging(1, path, "null", "null");
            listdir(path);
        } else {
            sprintf(cmd, "echo '%s/%s' >> listFiles.txt", name,entry->d_name);
            system(cmd);
        }
    }
    closedir(dir);
}


void *listfile(){
    char charMax [3];
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "max.txt");
    textfile = fopen (nama_file, "r"); fgets (charMax, 3, textfile); fclose(textfile);
    max = atoi(charMax);

    listdir("./files");

}

void *movefile(){
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "listFiles.txt");
    textfile = fopen(nama_file, "r");     
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-1] = '\0';
        char eks[MAX_LINE_LENGTH];
        sprintf(eks,"%s",get_filename_ext(line));
        for(int i = 0; eks[i]; i++){
            eks[i] = tolower(eks[i]);
        }
        if(check_ekstension(eks) == -1){

            sprintf(cmd, "cp '%s' ./categorized/other", line);
            system(cmd);
            create_thread_logging(2, "other", line,"./categorized/other" );
        }
        else{
            if(ekstensionDir[check_ekstension(eks)] < max){
                sprintf(cmd, "cp '%s' ./categorized/%s", line, eks);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s", eks);
                create_thread_logging(2, eks, line, buffer );
            }else{
                int num = ekstensionDir[check_ekstension(eks)] / max + 1;
                sprintf(buffer, "./categorized/%s %d", eks, num);
                if(access(buffer, F_OK) != 0){
                    create_thread_logging(3,buffer, "null", "null" );
                }
                sprintf(cmd, "mkdir -p './categorized/%s %d'", eks, num);
                system(cmd);

                sprintf(cmd, "cp '%s' './categorized/%s %d'", line, eks, num);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s %d", eks, num);
                create_thread_logging(2, eks, line, buffer );
            }
        }
    }
}

void *count_eks(){
    strcpy(ekstension[extCount], "other");
    DIR *directory;
    struct dirent *entry;
    directory = opendir("./categorized/other");

    create_thread_logging(1, "./categorized/other", "null", "null");

    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_REG) {
            ekstensionDir[extCount]++;
        }
    }

    closedir(directory);
    extCount++;

    for(int i=0;i<extCount; i++){
        for(int j=0; j<extCount-1;j++){
            if(ekstensionDir[j] > ekstensionDir[j+1]){
                int temp;
                temp = ekstensionDir[j];
                ekstensionDir[j] = ekstensionDir[j+1];
                ekstensionDir[j+1] = temp;

                strcpy(buffer, ekstension[j]);
                strcpy(ekstension[j], ekstension[j+1]);
                strcpy(ekstension[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<extCount; i++){
        printf("%s : %d\n", ekstension[i], ekstensionDir[i]);
    }
}

int main(){

    pthread_create(&threads[0], NULL, listfile, NULL);
    pthread_join(threads[0],NULL);

    pthread_create(&threads[0], NULL, createdir, NULL);
    pthread_join(threads[0],NULL);

    pthread_create(&threads[0], NULL, movefile, NULL);
    pthread_join(threads[0],NULL);

    pthread_create(&threads[0], NULL, count_eks, NULL);
    pthread_join(threads[0],NULL);

    system("rm listFiles.txt");
    system("rm -r files");

    return 0;
}