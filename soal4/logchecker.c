#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>

#define MAX_LINE_LENGTH 300

char dir[MAX_LINE_LENGTH][MAX_LINE_LENGTH];
int dirCount[MAX_LINE_LENGTH];
char eks[MAX_LINE_LENGTH][MAX_LINE_LENGTH];
int eksCount[MAX_LINE_LENGTH];
int indexDir = 0;
int indexEks = 0;

int count_accessed(){
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];
    int counter = 0;
    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");     
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "ACCESSED") != NULL) {
            counter++;
        }

    }
    fclose(textfile);
    return counter;
}

int check_dir(char dirs[] ){
    for(int i=0; i<indexDir;i++){
        if(strcmp(dirs, dir[i]) == 0)
            return i;
    }
    return -1;
}

int check_eks(char ekss[] ){
    for(int i=0; i<indexDir;i++){
        if(strcmp(ekss, eks[i]) == 0)
            return i;
    }
    return -1;
}

void all_dir_count(){

    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");     
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "MADE ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *pos = strstr(line, "MADE ");
            char *file = pos + strlen("MADE ");

            strcpy(dir[indexDir], file);
            dirCount[indexDir] = 0;
            indexDir++;
        }
    }
    fclose(textfile);

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r"); 
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "> ") != NULL) {
                line[strlen(line)-1] = '\0';
                char *pos = strstr(line, "> ");
                char *file = pos + strlen("> ");

                int num = check_dir(file);
                dirCount[num]++;
        }
    }
    fclose(textfile);


    for(int i=0;i<indexDir; i++){
        for(int j=0; j<indexDir-1;j++){
            if(dirCount[j] > dirCount[j+1]){
                int temp;
                temp = dirCount[j];
                dirCount[j] = dirCount[j+1];
                dirCount[j+1] = temp;

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, dir[j]);
                strcpy(dir[j], dir[j+1]);
                strcpy(dir[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<indexDir; i++){
        printf("%s : %d\n", dir[i], dirCount[i]);
    }
    
}

void all_eks_count(){
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    // strcpy(nama_file, "log.txt");
    // textfile = fopen(nama_file, "r");     
    // while(fgets(line, MAX_LINE_LENGTH, textfile)){
    //     if (strstr(line, "MOVED ") != NULL) {
    //         line[strlen(line)-1] = '\0';
    //         char *start = strstr(line, "MOVED ");
    //         char *en = strstr(start, " file :");
    //         char file[MAX_LINE_LENGTH];
    //         strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED "));
    //         file[en - start - strlen("MOVED ")] = '\0';

    //         if(check_eks(file) == -1)
    //         {
    //             strcpy(eks[indexEks], file);
    //             eksCount[indexEks] = 0;
    //             indexEks++;
    //         }
    //     }
    // }
    // fclose(textfile);

    strcpy(nama_file, "extensions.txt");
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-2] = '\0';
        strcpy(eks[indexEks], line);
        eksCount[indexEks] = 0;
        indexEks++;
    }
    fclose(textfile);
    strcpy(eks[indexEks], "other");
    eksCount[indexEks]=0;
    indexEks++;

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r"); 
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "MOVED ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *start = strstr(line, "MOVED ");
            char *en = strstr(start, " file :");
            char file[MAX_LINE_LENGTH];
            strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED "));
            file[en - start - strlen("MOVED ")] = '\0';

            int num = check_eks(file);
            eksCount[num]++;
        }
    }
    fclose(textfile);


    for(int i=0;i<indexEks; i++){
        for(int j=0; j<indexEks-1;j++){
            if(eksCount[j] > eksCount[j+1]){
                int temp;
                temp = eksCount[j];
                eksCount[j] = eksCount[j+1];
                eksCount[j+1] = temp;

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, eks[j]);
                strcpy(eks[j], eks[j+1]);
                strcpy(eks[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<indexEks; i++){
        printf("%s : %d\n", eks[i], eksCount[i]);
    }
}

int main(){
    printf("ACESSED COUNT : %d\n\n", count_accessed());

    printf("DIRECTORY COUNT : \n");
    all_dir_count();

    printf("\nEKSTENSION COUNT : \n");
    all_eks_count();
}