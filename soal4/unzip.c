#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>

pid_t child_id;
int status;

int main(){

    char file_url[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char nama_file[] = "hehe.zip";

    child_id = fork();
    if(child_id == 0) {
        execlp("wget", "wget", "-O", nama_file, file_url, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("unzip", "unzip", nama_file, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("rm", "rm", nama_file, NULL);
    }

    printf("Done Donwloading and Unzipping file\n");

}