#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>
#include "cJSON.h"

#define MAX_USER 3
#define SEM_NAME "/playlist"

const char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

struct msg_buffer
{
    long msg_type;
    char msg_text[50];
    int msg_uid;
} message;

char *decrypt_rot13(char *cipher)
{
    char *plaintext;
    int i, shift = 13;
    for (i = 0; cipher[i] != '\0'; i++)
    {
        if (isalpha(cipher[i]))
        {
            if (isupper(cipher[i]))
            {
                plaintext[i] = ((cipher[i] - 'A') + shift) % 26 + 'A';
            }
            else
            {
                plaintext[i] = ((cipher[i] - 'a') + shift) % 26 + 'a';
            }
        }
        else
        {
            plaintext[i] = cipher[i];
        }
    }
    plaintext[i] = '\0';

    return plaintext;
}

void decrypt_hex(const char *hex_string, char *output_string)
{
    int i, length;

    // hitung panjang string hex
    length = strlen(hex_string);

    // pastikan panjang string hex adalah bilangan genap
    if (length % 2 != 0)
    {
        fprintf(stderr, "Panjang string hex harus genap\n");
        return;
    }

    // konversi string hex menjadi string biasa
    for (i = 0; i < length; i += 2)
    {
        char buffer[3];
        buffer[0] = hex_string[i];
        buffer[1] = hex_string[i + 1];
        buffer[2] = '\0';

        output_string[i / 2] = (char)strtol(buffer, NULL, 16);
    }

    output_string[i / 2] = '\0';
}

int base64_decode(char *input, int input_len, char *output, int output_len)
{
    int i, j, k;
    unsigned char a, b, c, d;

    for (i = 0, j = 0; i < input_len && j < output_len; i += 4, j += 3)
    {
        a = input[i];
        b = input[i + 1];
        c = input[i + 2];
        d = input[i + 3];

        a = strchr(base64_chars, a) - base64_chars;
        b = strchr(base64_chars, b) - base64_chars;
        c = (c == '=') ? 0 : strchr(base64_chars, c) - base64_chars;
        d = (d == '=') ? 0 : strchr(base64_chars, d) - base64_chars;

        output[j] = (a << 2) | (b >> 4);
        if (c != 0xFF)
        {
            output[j + 1] = (b << 4) | (c >> 2);
            if (d != 0xFF)
            {
                output[j + 2] = (c << 6) | d;
            }
        }
    }

    return j;
}

char *decrypt_base64(char *base64)
{
    char *plaintext;
    int len;

    len = strlen(base64);
    if (base64[len - 1] == '\n')
    {
        base64[len - 1] = '\0';
        len--;
    }

    // Decode Base64 string to plaintext
    int plaintext_len = base64_decode(base64, len, plaintext, 1000);
    plaintext[plaintext_len] = '\0';

    return plaintext;
}

void Decrypt()
{
    pid_t pid;
    int status;

    pid = fork();

    if (pid == 0)
    {
        FILE *f = fopen("song-playlist.json", "r");
        FILE *ft = fopen("playlist.txt", "a");
        if (!f && !ft)
        {
            fprintf(stderr, "error open file");
        }
        // size dari file
        fseek(f, 0, SEEK_END);
        long file_size = ftell(f);
        rewind(f);

        // baca file
        char *buffer = (char *)malloc(file_size + 1);
        fread(buffer, 1, file_size, f);
        buffer[file_size] = '\0';
        fclose(f);

        // menguraikan json
        cJSON *data = cJSON_Parse(buffer);

        // menampilkan data
        if (cJSON_IsArray(data))
        {
            cJSON *item = NULL;
            cJSON_ArrayForEach(item, data)
            {
                cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
                cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");
                if (cJSON_IsString(method) && (method->valuestring != NULL))
                {
                    if (strcmp(method->valuestring, "rot13") == 0)
                    {
                        char *dec = decrypt_rot13(song->valuestring);
                        fputs(dec, ft);
                        fputs("\n", ft);
                    }
                    else if (strcmp(method->valuestring, "hex") == 0)
                    {
                        char dec[1000];
                        decrypt_hex(song->valuestring, dec);
                        fputs(dec, ft);
                        fputs("\n", ft);
                    }
                    else if (strcmp(method->valuestring, "base64") == 0)
                    {
                        char *dec = decrypt_base64(song->valuestring);
                        fputs(dec, ft);
                        fputs("\n", ft);
                    }
                }
            }
        }
        fclose(ft);
        fclose(f);
        cJSON_Delete(data);
        free(buffer);
        exit(1);
    }

    wait(&status);

    system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
    system("rm playlist.txt | mv sorted.txt playlist.txt");
}

void Play()
{
    char *cmd = "awk -F ' - ' 'tolower($1) ~ tolower(\"%s\") {print$%d}' playlist.txt";
    char buffer[50], song[50], singer[50][100], play[50][100];
    int i = 0, j = 5, k = 2;
    int index_singer = 0, index_play = 0;

    while (j != strlen(message.msg_text) + 1)
    {
        song[i] = message.msg_text[j];
        i++;
        j++;
    }
    song[i] = '\0';
    char song2[50];
    strcpy(song2, song);
    if (strlen(song) > 0)
    {
        sprintf(buffer, cmd, song, k--);
        // printf("BUFFER : %s\n", buffer);
        FILE *fp_singer = popen(buffer, "r");
        if (fp_singer == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        // get singer
        while (fgets(singer[index_singer], sizeof(singer[index_singer]), fp_singer) != NULL)
            index_singer++;

        sprintf(buffer, cmd, song2, k);
        // printf("BUFFER : %s\n", buffer);
        FILE *fp_play = popen(buffer, "r");
        if (fp_play == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        while (fgets(play[index_play], sizeof(play[index_play]), fp_play) != NULL)
            index_play++;
        fclose(fp_play);
        fclose(fp_singer);

        if (index_singer == 1)
        {
            // delete new line in singer
            int len_singer = strlen(singer[index_singer - 1]);
            if (singer[index_singer - 1][len_singer - 1] == '\n')
            {
                singer[index_singer - 1][len_singer - 1] = '\0';
            }
            // delete new line in play
            int len_play = strlen(play[index_singer - 1]);
            if (play[index_singer - 1][len_play - 1] == '\n')
            {
                play[index_singer - 1][len_play - 1] = '\0';
            }
            printf("USER <%d> PLAYING \"%s - %s\"\n", message.msg_uid, singer[index_singer - 1], play[index_singer - 1]);
        }
        else if (index_singer > 1)
        {
            printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", index_singer, song2);
            int index_list = 1;
            while (index_singer > 0)
            {
                // delete new line in singer
                int len_singer = strlen(singer[index_singer - 1]);
                if (singer[index_singer - 1][len_singer - 1] == '\n')
                {
                    singer[index_singer - 1][len_singer - 1] = '\0';
                }
                // delete new line in play
                int len_play = strlen(play[index_singer - 1]);
                if (play[index_singer - 1][len_play - 1] == '\n')
                {
                    play[index_singer - 1][len_play - 1] = '\0';
                }
                printf("%d. %s - %s\n", index_list++, singer[index_singer - 1], play[index_singer - 1]);
                index_singer--;
            }
        }
        else
        {
            printf("THERE IS NO SONG CONTAINING %s\n", song2);
        }
    }
    else
    {
        printf("UNKNOWN COMMAND\n");
    }
}

void Add()
{
    char *cmd = "awk 'tolower($0) ~ tolower(\"%s\") {print$0}' playlist.txt";
    int j = 4, i = 0;
    char song[50], buffer[50];

    while (j != strlen(message.msg_text) + 1)
    {
        song[i] = message.msg_text[j];
        i++;
        j++;
    }
    song[i] = '\0';
    // printf("%s\n", song);
    if (strlen(song) > 0)
    {
        char playlist[100];
        sprintf(buffer, cmd, song);
        // printf("BUFFER : %s\n", buffer);
        FILE *fp_song = popen(buffer, "r");
        if (fp_song == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        // find song
        pid_t pid;
        int status;

        // find song in play list
        fgets(playlist, sizeof(playlist), fp_song);

        // assign song in playlist
        if (strstr(playlist, song) != NULL)
        {
            printf("SONG ALREADY ON PLAYLIST\n");
        }
        else if ((song[0] != '\0') && strstr(playlist, song) == NULL)
        {
            pid = fork();
            if (pid == 0)
            {
                cmd = "echo %s >> playlist.txt";
                sprintf(buffer, cmd, song);
                system(buffer);
                exit(1);
            }
            wait(&status);

            system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
            system("rm playlist.txt | mv sorted.txt playlist.txt");

            printf("USER <%d> ADD %s\n", message.msg_uid, song);
        }
        else
        {
            printf("UNKNOWN COMMAND\n");
        }
    }
    else
    {
        printf("UNKNOWN COMMAND\n");
    }
}
/*
#######################################
            RUN THIS CODE
gcc -o stream stream.c cJSON.c -lrt
gcc -o user user.c -lrt
#######################################
*/

int main()
{
    key_t key;
    int msgid;
    int toEnd;
    sem_t *semaphore_rsc = sem_open(SEM_NAME, O_CREAT, 0644, MAX_USER);

    key = ftok("progfile", 65);
    msgid = msgget(key, 0666 | IPC_CREAT);

    printf("Ready to receive messages\n");
    int semVal, retrv, userID[2], index_user = 0;
    for (;;)
    {
        msgrcv(msgid, &message, sizeof(message), 0, 0);
        if(index_user <= 2 && userID[0] != message.msg_uid){
            userID[index_user] = message.msg_uid;
            index_user++;
        }else{
            // printf("USER ID : %d ARR[0] : %d ARR[1] : %d\n", message.msg_uid, userID[0], userID[1]);
            if(userID[0] != message.msg_uid && userID[1] != message.msg_uid){
                printf("STREAM SYSTEM OVERLOAD\n");
                continue;
            }
        }
        
        retrv = sem_getvalue(semaphore_rsc, &semVal);
        // printf("%s\n", message.msg_text);
        // printf("RETRV :%d\nSEMVAL :%d\n", retrv, semVal);
        
        if (sem_getvalue(semaphore_rsc, &semVal) == 0)
        {
            if (strcmp(message.msg_text, "DECRYPT") == 0)
            {
                printf("Decrypt\n");
                Decrypt();
            }
            else if (strcmp(message.msg_text, "LIST") == 0)
            {
                system("cat playlist.txt");
            }
            else if (strstr(message.msg_text, "PLAY") != NULL)
            {
                // printf("SIZE MSG :%d\n", (int)sizeof(message));
                // printf("UID :%d\nMSG_ID :%d\n", message.msg_uid, msgid);
                Play();
            }
            else if (strstr(message.msg_text, "ADD") != NULL)
            {
                Add();
            }
            else
            {
                printf("UNKNOWN COMMAND\n");
            }
            toEnd = strcmp(message.msg_text, "end");
            if (toEnd == 0)
                break;
        }
    }
    // sem_destroy(semaphore_rsc);
    sem_close(semaphore_rsc);
    msgctl(msgid, IPC_RMID, NULL);
    sem_unlink(SEM_NAME);
    printf("Done receiving messages.\n");
    return 0;
}