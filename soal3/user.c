// C Program for Message Queue (Writer Process)
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>

#define MAX 50
#define SEM_NAME "/playlist"

// structure for message queue
struct mesg_buffer {
	long mesg_type;
	char mesg_text[MAX];
	int mesg_uid;
} message;

int main(){
	key_t key;
	int msgid;
    int len, retval;
	sem_t *semaphore_rsc = sem_open(SEM_NAME, 0);
	if(semaphore_rsc == SEM_FAILED){
		perror("semopen ");
		exit(1);
	}
	key = ftok("progfile", 65);
	
	// retval = sem_wait(semaphore_rsc);
	// printf("sem wait: %d\n", retval);
	
	// if(sem_getvalue(semaphore_rsc, &retval) == 0){
	// 	printf("sem value: %d\n", (int)retval);
	// }
	
	
	msgid = msgget(key, 0666 | IPC_CREAT);
	message.mesg_type = 1;
	message.mesg_uid = getpid();

	printf("Ready to send messages.\n");
    printf("Enter lines of text, ^D to quit\n");
	while(fgets(message.mesg_text,MAX,stdin) != NULL){
        len = strlen(message.mesg_text);
        if(message.mesg_text[len-1] == '\n') 
            message.mesg_text[len-1] = '\0';
	    
		msgsnd(msgid, &message, (int)sizeof(message), 0);
    }
    //end
    strcpy(message.mesg_text, "end");
    len = strlen(message.mesg_text);
    if(message.mesg_text[len-1] == '\n') 
        message.mesg_text[len-1] = '0';
	msgsnd(msgid, &message, len+1, 0);

	// display the message
	printf("Done sending messages.\n");
	sem_post(semaphore_rsc);
	sem_close(semaphore_rsc);

	return 0;
}
