#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#define BUFFER_SIZE 256
#define MAX_TREE_HT 50

#define INPUT_FILE "file.txt"
#define OUTPUT_FILE "bits_compressed.txt"
#define ASCII_BITS 8


// Huffman Coding in C by GeeksforGeeks: https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/
struct MinHNode
{
    char item;
    unsigned freq;
    struct MinHNode *left, *right;
};

struct MinHeap
{
    unsigned size;
    unsigned capacity;
    struct MinHNode **array;
};

struct MinHNode *newNode(char item, unsigned freq)
{
    struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

    temp->left = temp->right = NULL;
    temp->item = item;
    temp->freq = freq;

    return temp;
}

struct MinHeap *createMinH(unsigned capacity)
{
    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

    minHeap->size = 0;

    minHeap->capacity = capacity;

    minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
    return minHeap;
}

void swapMinHNode(struct MinHNode **a, struct MinHNode **b)
{
    struct MinHNode *t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx)
{
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx)
    {
        swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

int checkSizeOne(struct MinHeap *minHeap)
{
    return (minHeap->size == 1);
}

struct MinHNode *extractMin(struct MinHeap *minHeap)
{
    struct MinHNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode)
{
    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq)
    {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap)
{
    int n = minHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}

int isLeaf(struct MinHNode *root)
{
    return !(root->left) && !(root->right);
}

struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size)
{
    struct MinHeap *minHeap = createMinH(size);

    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(item[i], freq[i]);

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size)
{
    struct MinHNode *left, *right, *top;
    struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

    while (!checkSizeOne(minHeap))
    {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}

void printArray(int arr[], int n)
{
    int i;
    for (i = 0; i < n; ++i)
        printf("%d", arr[i]);

    printf("\n");
}

void printHCodes(struct MinHNode *root, int arr[], int top)
{
    static char huffmanCode[MAX_TREE_HT];
    if (root->left)
    {
        arr[top] = 0;
        printHCodes(root->left, arr, top + 1);
    }
    if (root->right)
    {
        arr[top] = 1;
        printHCodes(root->right, arr, top + 1);
    }
    if (isLeaf(root))
    {
        printf("  %c   | ", root->item);
        for (int i = 0; i < top; ++i)
        {
            printf("%d", arr[i]);
            huffmanCode[i] = arr[i] + '0';
        }
        huffmanCode[top] = '\0';
        printf("\n");
    }
}

void encodeCharacter(struct MinHNode *root, char character, char huffmanCode[], int index, int *found)
{
    if (root == NULL)
    {
        return;
    }
    if (root->left == NULL && root->right == NULL)
    {
        if (root->item == character)
        {
            huffmanCode[index] = '\0';
            *found = 1;
        }
        return;
    }
    huffmanCode[index] = '0';
    encodeCharacter(root->left, character, huffmanCode, index + 1, found);
    if (*found == 0)
    {
        huffmanCode[index] = '1';
        encodeCharacter(root->right, character, huffmanCode, index + 1, found);
    }
}

char* HuffmanCodes(char item[], int freq[], int size, char str[])
{
    struct MinHNode *root = buildHuffmanTree(item, freq, size);

    static char huffmanCode[MAX_TREE_HT];
    int index = 0;
    char* result = (char*) malloc(sizeof(char) * strlen(str) * MAX_TREE_HT);
    strcpy(result, "");
    for (int i = 0; str[i] != '\0'; ++i)
    {
        int found = 0;
        encodeCharacter(root, str[i], huffmanCode, 0, &found);
        if (!found)
        {
            printf("Cannot find Huffman code for character: %c\n", str[i]);
            return NULL;
        }
        strcat(result, huffmanCode);
    }
    return result;
}

int main()
{
    // Create pipes
    int parent_to_child_pipe[2];
    int child_to_parent_pipe[2];
    if (pipe(parent_to_child_pipe) < 0 || pipe(child_to_parent_pipe) < 0)
    {
        printf("Failed to create pipes\n");
        exit(1);
    }

    // Fork process
    pid_t pid = fork();

    if (pid < 0)
    {
        printf("Failed to fork process\n");
        exit(1);
    }
    else if (pid == 0)
    {
        // Child process

        // Close unused ends of the 1st pipe
        close(parent_to_child_pipe[1]);

        // Read size of arrays from parent process
        int size;
        if (read(parent_to_child_pipe[0], &size, sizeof(size)) < 0)
        {
            printf("Failed to read size from parent process\n");
            exit(1);
        }

        // Read arrays from parent process
        char array1[size];
        int array2[size];
        if (read(parent_to_child_pipe[0], array1, sizeof(array1)) < 0 ||
            read(parent_to_child_pipe[0], array2, sizeof(array2)) < 0)
        {
            printf("Failed to read arrays from parent process\n");
            exit(1);
        }

        // Open text files that need to be compressed
        FILE *inputFile = fopen(INPUT_FILE, "rt");
        if (NULL == inputFile)
        {
            printf("ERROR: cannot open the file: %s\n", INPUT_FILE);
            return -1;
        }

        // Open text files that already match the requirement
        FILE *outputFile = fopen(OUTPUT_FILE, "wt");
        if (NULL == outputFile)
        {
            printf("ERROR: cannot open the file: %s\n", OUTPUT_FILE);
            return -1;
        }

        // Write text files that match the requirement
        char line[1000];
        while (fgets(line, sizeof(line), inputFile))
        {
            for (int i = 0; line[i] != '\0'; i++)
            {
                if (isalpha(line[i]) && islower(line[i]))
                {
                    fputc(toupper(line[i]), outputFile);
                }
                else if (isalpha(line[i]) && isupper(line[i]))
                {
                    fputc(line[i], outputFile);
                }
            }
        }

        // Close the IO
        fclose(inputFile);
        fclose(outputFile);

        // Convert text files to a string
        char str[1000];
        FILE *fp = fopen(OUTPUT_FILE, "r");
        fgets(str, sizeof(str), fp);
        fclose(fp);

        // Close unused ends of the 2nd pipe
        close(child_to_parent_pipe[0]);

        // Compress the string/text with the Huffman Algorithm
        char* encodedStr = HuffmanCodes(array1, array2, size, str);

        // printf("This is a string in the parrent process: %s\n", encodedStr);

        // Send the comprresed string/text to parrent process
        if (write(child_to_parent_pipe[1], encodedStr, strlen(encodedStr) + 1) < 0)
        {
            printf("Failed to send data to parent process\n");
            exit(1);
        }

        // Close pipes
        close(parent_to_child_pipe[0]);
        close(child_to_parent_pipe[1]);

        // Exit child process
        exit(0);
    }
    else
    {
        // Open input file
        FILE *input_file = fopen(INPUT_FILE, "r");
        if (input_file == NULL)
        {
            printf("Failed to open input file\n");
            exit(1);
        }

        // Initialize frequency table
        int freq_table[26] = {0};

        // Read input file and update frequency table
        int c;
        while ((c = fgetc(input_file)) != EOF)
        {
            if (isalpha(c))
            {
                c = toupper(c);
                freq_table[c - 'A']++;
            }
        }

        // Close input file
        fclose(input_file);

        // Create arrays to store characters and their frequencies
        int size = 0;
        for (int i = 0; i < 26; i++)
        {
            if (freq_table[i] > 0)
            {
                size++;
            }
        }
        char array1[size];
        int array2[size];
        int j = 0;
        for (int i = 0; i < 26; i++)
        {
            if (freq_table[i] > 0)
            {
                array1[j] = 'A' + i;
                array2[j] = freq_table[i];
                j++;
            }
        }

        // Close unused ends of the 1st pipe
        close(parent_to_child_pipe[0]);

        // Send size and arrays to child process
        if (write(parent_to_child_pipe[1], &size, sizeof(size)) < 0 ||
            write(parent_to_child_pipe[1], array1, sizeof(array1)) < 0 ||
            write(parent_to_child_pipe[1], array2, sizeof(array2)) < 0)
        {
            printf("Failed to send data to child process\n");
            exit(1);
        }

        // Wait for child process to exit
        int status;
        waitpid(pid, &status, 0);

        // Open text file that already match the requirement
        FILE *fp;
        char filename[] = "bits_compressed.txt";
        int countBefore = 0;
        char ch;

        fp = fopen(filename, "r");

        if (fp == NULL)
        {
            printf("Unable to open file\n");
            return 0;
        }

        // Count the total character inside the text file
        while ((ch = fgetc(fp)) != EOF)
        {
            countBefore++;
        }

        fclose(fp);

        // printf("Number of characters in file %s is %d\n", filename, count);

        // Close unused ends of 2nd pipe
        close(child_to_parent_pipe[1]);
        
        // Read the encoded string from the child process
        char encodedStr[3000];
        int bytesRead = read(child_to_parent_pipe[0], encodedStr, sizeof(encodedStr));
        if (bytesRead < 0)
        {
            printf("Failed to read string from child process\n");
            exit(1);
        }

        // Trim the encoded string to the actual number of bytes read
        encodedStr[bytesRead] = '\0';

        // printf("\nThis is a string in the parrent process: %s\n", encodedStr);

        int countAfter = 0;  
      
        // Count the total character inside the comprresed string/text
        for(int i = 0; i < strlen(encodedStr); i++) {  
            if(encodedStr[i] != ' ')  
                countAfter++;  
        }  
        
        // printf("Total number of characters in a string: %d", countAfter);  
    
        // Comparing the total bits before compressing and after compressing
        int totalBitBefore = countBefore * ASCII_BITS;
        printf("Before Compression:\n");
        printf("Total bit: %d\n\n", totalBitBefore);

        int totalBitAfter = countAfter;
        printf("After Compression:\n");
        printf("Total bit: %d\n", totalBitAfter);


        // Close pipes
        close(parent_to_child_pipe[1]);
        close(child_to_parent_pipe[0]);

        // Exit parent process
        exit(0);
    }
}
